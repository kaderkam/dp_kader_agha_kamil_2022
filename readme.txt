This master thesis aims to survey content-based recommendation systems based on image data, state-of-the-art convolutional neural networks for feature extraction from images, and possibilities of producing recommendations based on more than one image of a single item, so-called multiple instance learning. Subsequently, new content-based recommendation methods that use one and more images of a single item and incorporate interactions from users behavior to improve the recommendations were described. Several prototypes were implemented based on the state-of-the-art convolutional neural networks, and compared in offline tests in their ability to extract important features for recommendations on cold start problem. Finally, four of the models were compared in the online A/B test against each other. The results showed that the incorporation of user interactions and more images into the recommender system improved the measured click-through rate metric.


CD directory tree:

readme.txt  ............................  the file with CD contents description.
results ............................... results of our experiments in text form.
src ............................................  the directory of source codes.
    implementation ....................................  implementation sources.
    thesis .................  the directory of LaTeX source codes of the thesis.
text ...............................................  the thesis text directory.
    DP_Kader_Agha_Kamil_2022.pdf ...............  the thesis text in PDF format.
