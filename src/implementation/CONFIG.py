PATH_TO_IMGES = "images/" # Path to images
MODELS_SAVE_PREFIX = "./models_checkpoint/" # where to save trained models
SAVE_MODEL_RESULTS = 'trained_NN.json' # where to save ONLY results of trained models
KERAS_TUNER_PATH = './TUNER/' # where to save KT progress

PATH_TO_LOGS = './logs/training_log.log' # Path to save logs
KNN_VISUALIZATION = 'pred_KNN_pics/' # Where to save visualization of KNN recommendations

