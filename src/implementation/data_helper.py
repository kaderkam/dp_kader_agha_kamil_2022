import numpy as np
import pandas as pd
import tensorflow as tf
from CONFIG import *
AUTOTUNE = tf.data.experimental.AUTOTUNE
np.random.seed(1234)
tf.random.set_seed(1234)

class DatasetsHandler():
    def load_datasets(self, MIL=False):
        """Load whole dataset

        Parameters
        ----------
        MIL : bool, optional, default: False
            If True, filter only main images to lower memory need.
            
        Returns
        -------
        (d1, d2, d3, d4, d5)
            d1 = final merge(dataset for training), d2 = filtered dataset for training for one-to-one,
            d3 = paths to images for prediction & evaluation, d4 = interaction matrix valid dataframe,
            d5 = interaction matrix test dataframe.
        """
        
        # Load final merge
        final_merge = pd.read_pickle("./final_merge.pkl")

        # Filter only unique images
        final_merge_one = final_merge[(final_merge["property"] == 'additional_images')]
        final_merge_one = final_merge_one[(final_merge_one["image_number"] == 0)]
        print('Total learning samples:', len(final_merge_one))

        # Predict embedding of those images and recommend to them some other items
        pics_paths = pd.read_pickle("./only_functional_paths.pkl")
        if not MIL:
            pics_paths = pics_paths[pics_paths["property"] == 'main_image'] # keep low otherwise takes too much memory
        pics_paths_unique = pics_paths # Predict embedding of those images and recommend to them some other items

        # build USER x ITEM interaction matrix and see recall& recommended items
        raw_interaction_valid = pd.read_pickle(PATH_TO_IMGES + "/raw_interactions_valid.pkl")
        raw_interaction_test  = pd.read_pickle(PATH_TO_IMGES + "/raw_interactions_test.pkl")

        # Make it ready for measure_leave_one_out (need unique items and users in table, with lower and upper values)
        raw_interaction_valid = raw_interaction_valid.groupby(['user_id', 'item_id'], observed=True)
        raw_interaction_valid = raw_interaction_valid['value'].sum().clip(lower=-1, upper=1).reset_index()
        print('Interactions: (valid): ', len(raw_interaction_valid))

        # Make it ready for measure_leave_one_out (need unique items and users in table, with lower and upper values)
        raw_interaction_test = raw_interaction_test.groupby(['user_id', 'item_id'], observed=True)
        raw_interaction_test = raw_interaction_test['value'].sum().clip(lower=-1, upper=1).reset_index()
        print('Interactions: (test):  ', len(raw_interaction_test))

        return final_merge, final_merge_one, pics_paths_unique, raw_interaction_valid, raw_interaction_test

    def splitter(self, df, split_ratio):
        # Split dataframe in two with split_ration parameter.
        
        split=(int)(len(df)*split_ratio)
        
        x_1 = df[split:]
        x_2 = df[:split]
        
        return x_1, x_2
        
    def split_train_valid(self, PATH_TO_IMGES, pd_data, ratio):
        """Prepare train and valid datasets by splitting pd_data with ratio.

        Parameters
        ----------
        PATH_TO_IMGES : str
            Path to images in dataframe.
        pd_data : pandas dataframe
            Pandas dataframe with path to image and ALS embedding.
        ratio : float
            Ratio between dataset to split them into two new datasets.       
            
        Returns
        -------
        (d1, d2, d3, d4)
            d1 = input train dataset, d2 = output train dataset
            d3 = input valid dataset, d3 = output valid dataset
        """
        test_image_embedd_a = []
        test_image_location_a = []

        # output
        test_image_embedd_a = pd_data['embedding'].apply(lambda x: np.array(x, dtype=np.float32))
        test_image_embedd_a =  np.array(test_image_embedd_a.tolist())
        
        # input
        test_image_location_a = pd_data['filename'].apply(lambda x: PATH_TO_IMGES + '/' +x).tolist()

        x_train_train, x_train_valid = self.splitter(test_image_location_a, ratio)
        y_train_train, y_train_valid = self.splitter(test_image_embedd_a, ratio)

        return x_train_train, y_train_train, x_train_valid, y_train_valid

    def make_dataset(self, PATH_TO_IMGES, pd_data, split_ratio):
        """Prepare train and valid datasets by splitting pd_data with split_ratio.

        Parameters
        ----------
        PATH_TO_IMGES : str
            Path to images in dataframe.
        pd_data : pandas dataframe
            Pandas dataframe with path to image and ALS embedding.
        split_ratio : float
            Ratio between dataset to split them into two new datasets.       
            
        Returns
        -------
        array
            In format [d1, d2, d3, d4]
            d1 = input train dataset, d2 = output train dataset
            d3 = input valid dataset, d3 = output valid dataset
        """
        
        #Split the dataset into training and validation
        x_train_train, y_train_train, x_train_valid, y_train_valid = self.split_train_valid(PATH_TO_IMGES, pd_data, split_ratio)

        print('Train_train count:', len(x_train_train)) # train model
        print('Train_valid count:', len(x_train_valid)) # validate model
        print('Total one-to-one: ', len(x_train_train) + len(x_train_valid))

        return [x_train_train, y_train_train, x_train_valid, y_train_valid]
    
    # MANY-TO-ONE =================================================
    def get_top_K_imgs_from_dataset(self, final_merge, TOP_K_IMGS):
        # Filter top-K images form the dataset
        
        final_merge = final_merge.loc[final_merge['property'] == 'additional_images']
        final_merge = final_merge[final_merge['image_number'].isin([x for x in range(TOP_K_IMGS)])] 
        final_merge = final_merge.reset_index()
        return final_merge
    
    def re_number_items(self, x):
        # Change numbers of item
        
        if x['item_id'] != self.last_item:
            self.last_item = x['item_id']
            self.help_c = 0

        x['image_number'] = self.help_c
        self.help_c += 1

        return x 
        
    def reindex_img_number(self, final_merge):
        """Reindex image numbers in case of error.

        Parameters
        ----------
        final_merge : pandas.DataFrame
            Dataframe with number of images.

        Returns
        -------
        pandas.DataFrame
            Reindexed image numbers
        """
        self.last_item = None 
        self.help_c = 0
        
        final_merge = final_merge.sort_values(by=['item_id', 'image_number'])
        final_merge = final_merge.apply(lambda x: self.re_number_items(x), axis=1)
        final_merge = final_merge.reset_index(drop=True)
        return final_merge
    
    def get_img_weights(self, img_numbers):
        """Count image weights (i.e., number of images from each item)

        Parameters
        ----------
        img_numbers : array
            Array with number of images. Example: img_numbers = [0,1,0,1,2,0,0,0,1,2,0,0,1]

        Returns
        -------
        array
            Array of weights of corresponding image.
        """
        img_weights = []
        last_number = -1
        max_number = None
        
        for i in range(len(img_numbers)-1, -1, -1):
            if img_numbers[i] == last_number-1:
                img_weights.append(1/max_number)
            else:
                max_number = img_numbers[i] + 1
                img_weights.append(1/max_number)

            last_number = img_numbers[i] 

        img_weights.reverse()

        return img_weights
    
    def split_weights(self, img_weights, SPLIT_RATIO):
        # Splitting weights of images into two arrays with SPLIT_RATIO

        data_size=len(img_weights)
        split_index=(int)(data_size*SPLIT_RATIO)
        
        img_weights_t = img_weights[split_index:] # train weights
        img_weights_v = img_weights[:split_index] # valid weights
        img_weights_out = [img_weights_t, img_weights_v]
        
        print('Total weights train:', len(img_weights_t), '(should equal: Train_train count)')
        print('Total weights valid:', len(img_weights_v), '(should equal: Train_valid count)')
        print('Total weights:    ', len(img_weights), '(should equal: Total one-to-one)')
        
        return img_weights_out
    
    def filter_top_K_imgs(self, input_frame, TOP_K_IMGS):
        # Prepare data for many-to-one and MIL models 
        
        # Filter correct images
        input_frame = self.get_top_K_imgs_from_dataset(input_frame, TOP_K_IMGS)
        
        # Reindex them
        input_frame = self.reindex_img_number(input_frame)
        input_frame = input_frame[['filename', 'item_id', 'embedding', 'image_number']]
        return input_frame
    
    def get_many_to_one(self, input_frame, TOP_K_IMGS, SPLIT_RATIO):
        """Prepare dataset and weights for many-to-one and MIL training.

        Parameters
        ----------
        input_frame : pandas.DataFrame
            Dataframe must contain columns: 'filename', 'embedding', 'image_number'.
        TOP_K_IMGS : int
            Number of maximum number of images to use from each item.
        SPLIT_RATIO : float
            Float number determins how to split datasets into two (trian & valid) datasets.    

        Returns
        -------
        (datasets, img_weights)
            Datasets if dataframe of filtered images. 'img_weights' is array of weights of corresponding img.
        """
        # filter images
        input_frame = self.filter_top_K_imgs(input_frame, TOP_K_IMGS)
        datasets = self.make_dataset(PATH_TO_IMGES, input_frame, SPLIT_RATIO)
        
        # get weights of each img
        img_weights = self.get_img_weights(input_frame['image_number'].to_numpy())        
        img_weights = self.split_weights(img_weights, SPLIT_RATIO)

        return datasets, img_weights

    def make_predict_dataset(self, pics_paths_unique, TOP_K_IMGS):
        """Prepare dataset for MIL prediction (without ALS dataset,
           we just add more images to the main_image if item has some).

        Parameters
        ----------
        pics_paths_unique : pandas.DataFrame
            Dataframe must contain columns: 'item_id', 'filename', 'property', 'image_number'.
        TOP_K_IMGS : int
            Number of maximum number of images to use from each item.
            
        Returns
        -------
        datasets
            Dataset with filtered images. 
        """
        
        # Fitler first images
        df1 = pics_paths_unique[pics_paths_unique["property"] == 'main_image']
        corresponding_items = pics_paths_unique['item_id'].isin(df1['item_id'])
        needed_items = pics_paths_unique[corresponding_items]

        # Prepare datasets with more images (additional_images)
        frames = [df1]
        for i in range(TOP_K_IMGS-1):
            additional_items = needed_items[needed_items['property'] == 'additional_images']
            frames.append(additional_items[needed_items['image_number'] == i+1])

        # Append everything together
        pics_paths_predict = pd.concat(frames).sort_values(by=['item_id', 'image_number'])
        pics_paths_predict = self.reindex_img_number(pics_paths_predict)
        return pics_paths_predict
    
    # TF DATASET =================================================
    def parse_data(self, x,y, IMG_SIZE, z=None):
        """Load and prepare data into GPU memory for TensorFlow.

        Parameters
        ----------
        x : str
            Path to image.
        y : array
            Target ALS embedding for training.
        IMG_SIZE : int
            Required dimension of image.
        z : int, optional, defualt = None
            Weight of image for many-to-one teaching.    
            
        Returns
        -------
        (x, y, z)
            Where x is parsed image in TensorFlow, y is target ALS embedding, y is optional weight.
        """
        
        # Load and prepare image.
        image_string  = tf.io.read_file(x)
        image_decoded = tf.image.decode_jpeg(image_string, channels=3)
        image_resized = tf.image.resize(image_decoded, (IMG_SIZE, IMG_SIZE))
        image_standard = tf.image.per_image_standardization(image_resized) # (image/127.5) - 1
        image = tf.cast(image_standard, tf.float32)

        if z is None:
            return image,y
        else:
            return image,y, z

    def create_tf_ds(self, x,y, BATCH_SIZE, IMG_SIZE, img_weights = None):
        # Create tf.data.Dataset with corresponding setting.
        
        if img_weights is None: # one-to-one
            ds = tf.data.Dataset.from_tensor_slices((x,y))
            ds = ds.map(lambda x,y: self.parse_data(x,y,IMG_SIZE))
        else: # many-to-one
            ds = tf.data.Dataset.from_tensor_slices((x,y,img_weights))
            ds = ds.map(lambda x,y,z: self.parse_data(x,y,IMG_SIZE, z))

        ds = ds.batch(BATCH_SIZE) # Set batch-size
        ds = ds.prefetch(buffer_size=AUTOTUNE)
        return ds
    
    def get_tf_ds(self, datasets, top_dimen, BATCH_SIZE, img_weights=None):
        # get tf.data.Datasets of train&valid dataframes. (eventually with weights)
        if img_weights is None:
            img_weights = [None, None]
        train_train_ds = self.create_tf_ds(datasets[0],datasets[1], BATCH_SIZE, top_dimen, img_weights[0])
        train_valid_ds = self.create_tf_ds(datasets[2],datasets[3], BATCH_SIZE, top_dimen, img_weights[1])  
        return train_train_ds, train_valid_ds
    
    def get_tf_ds_without_target(self, pics_paths, IMG_SIZE, BATCH_SIZE):
        # get tf.data.Datasets of dataframes without ALS embedding.
        pics_paths_np = pics_paths['filename'].apply(lambda x: PATH_TO_IMGES + '/' + x).tolist() # Source image (input)
        pics_zero_embed = np.full((len(pics_paths_np), 1), 0) # dummy target dataset, can be 1 since we need only predict values 

        return self.create_tf_ds(pics_paths_np, pics_zero_embed, BATCH_SIZE, IMG_SIZE)
    
    # MIL DATASETS ====================================================
    def get_dataset_simple_phase2(self, ALS, PHASE2_BATCH, IMG_SIZE, dummy_target=False):    
        # Prepare tf.data.Dataset for Simple MIL phase 2 (2nd DNN).
        # If dummy_target=True for evaluation, else for training.
        
        x = np.array(ALS['embedd_simple'].apply(lambda x: np.array(x)).tolist()).astype('float32')
        if dummy_target:
            y = np.full((len(ALS), 1), 0)  # can be 1, only dummy value
        else:
            # Target ALS embedding
            y = np.array(ALS['embedding'].apply(lambda x: np.array(x)).tolist()).astype('float32')
        
        ds = tf.data.Dataset.from_tensor_slices((x,y))
        ds = ds.batch(PHASE2_BATCH)
        ds = ds.prefetch(buffer_size=AUTOTUNE)
        return ds
