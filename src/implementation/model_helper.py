import tensorflow as tf
import keras_tuner as kt
from CONFIG import *
tf.random.set_seed(1234)

class CModels:

    def __init__(self):
        self.keras_tuner_model_name = None
    
    def custom_loss_function(self, mse_ratio, cos_ratio):
        """Return custom loss function

        Parameters
        ----------
        mse_ratio : float
            The ratio of MSE
        cos_ratio : float
            The ratio of COS

        Returns
        -------
        loss function
        """
        def loss(y_true, y_pred): 
            mse = tf.keras.losses.MeanSquaredError()
            cos = tf.keras.losses.CosineSimilarity(axis=1) 
            
            mse_res = mse_ratio * mse(y_true, y_pred) # minimizing to 0
            cos_res = cos_ratio * (cos(y_true, y_pred) + tf.constant(1.0)) # minimizing to 0 (thanks to +1, otherwise to -1)
            return mse_res + cos_res
        return loss
    
    def compile_model(self, model, my_optimizer=None, loss_ratio=[0.01, 1]):
        """Compile model

        Parameters
        ----------
        my_optimizer : tf.keras.optimizers, optional
            The ratio of MSE
        loss_ratio : array, optional
            Array of [MSE_ratio, COS_ratio]

        Returns
        -------
        None
        """
        if my_optimizer is None:
            my_optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001)

        model.compile(optimizer=my_optimizer,\
                loss=self.custom_loss_function(mse_ratio=loss_ratio[0], cos_ratio=loss_ratio[1])) 
    
    def get_correct_model(self, hp_model):
        """Return the corresponding model, model_name, top_dimen, where top_dimen is input size of model (ex. 224, 224, 3)

        Parameters
        ----------
        hp_model : str
            Name of the model in format MODEL_NAME_X, where X is number of ommited layers
            
        Returns
        -------
        (model, model_name, top_dimen) 
        """
        
        if hp_model == 'DenseNet121_1':
            return self.get_DenseNet121_model(1)
        if hp_model == 'DenseNet169_1':
            return self.get_DenseNet169_model(1)
        if hp_model == 'DenseNet201_1':
            return self.get_DenseNet201_model(1)
        if hp_model == 'efficientNetV2B0_2':
            return self.get_efficientNetV2B0_model(2)
        if hp_model == 'efficientNetV2B1_2':
            return self.get_efficientNetV2B1_model(2)
        if hp_model == 'efficientNetV2B2_2':
            return self.get_efficientNetV2B2_model(2)
        if hp_model == 'efficientNetV2B3_2':
            return self.get_efficientNetV2B3_model(2)
        if hp_model == 'efficientNetV2L_2':   
            return self.get_efficientNetV2L_model(2)
        if hp_model == 'VGG16_3':
            return self.get_VGG16_model_only(3)
        if hp_model == 'VGG19_3':
            return self.get_VGG19_model_only(3)
        
        print("ERROR - undefined model in keras search")
        return self.get_DenseNet201_model(1)
    
    def get_model_tuner(self, hp):
        # Keras tuner hyperparameters specification for one-to-one model.
        
        # get correct model
        main_model, model_name, top_dimen = self.get_correct_model(self.keras_tuner_model_name)

        # Define end layers
        flatten_layer = tf.keras.layers.Flatten()
        prediction_layer = tf.keras.layers.Dense(1024, activation='linear', name='predictions')
        
        model = tf.keras.Sequential([
            main_model,
            flatten_layer,
        ], name=model_name)

        # dense_layers --------------------
        hp_dense = hp.Choice('dense_layers', values=[0, 1, 2])
        hp_N_multiple = hp.Choice('dense_multiple', values=[1, 2, 3])
        for i in range(hp_dense):
            model.add(tf.keras.layers.Dense(1024*hp_N_multiple, activation='relu', name='fc' + str(i)))
        model.add(prediction_layer)
        model.layers[0].trainable = False

        # optimziers + LR --------------------
        hp_LR = hp.Float('learning_rate', min_value=1e-5, max_value=1e-2, sampling='LOG')
        optimizer = tf.keras.optimizers.Adam(learning_rate=hp_LR)
        
        # compile model
        self.compile_model(model, optimizer)
        
        return model
    
    
    # KERAS TUNER FOR MIL SIMPLE
    def run_tuner(self, main_function, train_ds, valid_ds, model_name, EPOCHS_KT, overwrite=True):
        """Run KERAS TUNER on specific hyperparameters

        Parameters
        ----------
        main_function : function
            Function with defined hyperparameters
        train_ds : tf.data.Dataset
            Train dataset
        valid_ds : tf.data.Dataset
            Valid dataset
        model_name : str
            Name of the model
        EPOCHS_KT : int
            Number of epochs to run tuner
        overwrite : bool, optional
            True - overwrite last search files and start from beggining, False - continue from last save
            
        Returns
        -------
        Kerar_Tuner
            Return finished tuner
        """
        tuner = kt.Hyperband(main_function,
                         objective=kt.Objective("val_loss", direction="min"), # val_loss is not available
                         max_epochs=EPOCHS_KT,
                         factor=2, 
                         directory=KERAS_TUNER_PATH,
                         project_name=model_name,
                         seed=1234,
                         overwrite=overwrite,)
        tuner.search(train_ds, epochs=EPOCHS_KT, steps_per_epoch=None, validation_data=valid_ds)
        tuner.results_summary(1)
        return tuner
    
    
    # LOADING MODELS =============================== 
    def load_TUNER_model(self, model_name, main_function):
        # Load model from KT 
        self.keras_tuner_model_name = model_name
        tuner1 = kt.Hyperband(
            main_function,
            objective='val_loss',
            directory=KERAS_TUNER_PATH,
            project_name=model_name)

        tuner1.reload()
        return tuner1.get_best_models(num_models=1)[0]
    
    def load_my_model_with_custom_loss(self, save_path, LR):
        # Load only saved model
        #path_to_model = 'models_checkpoint/VGG16_3'
        model = tf.keras.models.load_model(MODELS_SAVE_PREFIX + save_path,  compile=False)
        optimizer = tf.keras.optimizers.Adam(learning_rate=LR) # FROM TUNER
        self.compile_model(model, optimizer,)
        return model

    # VGG MODELS =================================================
    
    def get_VGG16_model_only(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return VGG16 model and its specifications
        model_name = 'VGG16_' + str(last_layer_count)
        model_full = tf.keras.applications.VGG16(weights='imagenet', include_top=True, input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_VGG19_model_only(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return VGG19 model and its specifications
        model_name = 'VGG19_' + str(last_layer_count)
        model_full = tf.keras.applications.vgg19.VGG19(include_top=True, weights='imagenet',input_shape=top_dimen)

        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    # EFFICIENT MODELS ===============================================
    
    def get_efficientNetB0_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return efficientNetB0 model and its specifications
        model_name = 'efficientNetB0_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB0(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB1_model(self, last_layer_count, top_dimen = (240, 240, 3)):
        # Return efficientNetB1 model and its specifications
        model_name = 'efficientNetB1_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB1(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB2_model(self, last_layer_count, top_dimen = (260, 260, 3)):
        # Return efficientNetB2 model and its specifications
        model_name = 'efficientNetB2_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB2(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB3_model(self, last_layer_count, top_dimen = (300, 300, 3)):
        # Return efficientNetB3 model and its specifications
        model_name = 'efficientNetB3_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB3(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB4_model(self, last_layer_count, top_dimen = (380, 380, 3)):
        # Return efficientNetB4 model and its specifications
        model_name = 'efficientNetB4_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB4(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB5_model(self, last_layer_count, top_dimen = (456, 456, 3)):
        # Return efficientNetB5 model and its specifications
        model_name = 'efficientNetB5_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB5(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB6_model(self, last_layer_count, top_dimen = (528, 528, 3)):
        # Return efficientNetB6 model and its specifications
        model_name = 'efficientNetB6_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB6(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetB7_model(self, last_layer_count, top_dimen = (600, 600, 3)):
        # Return efficientNetB7 model and its specifications
        model_name = 'efficientNetB7_' + str(last_layer_count)
        model_full = tf.keras.applications.EfficientNetB7(weights="imagenet", include_top=True, input_shape=top_dimen, classifier_activation='linear')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    # EFFICIENT_V2 MODELS ===============================================
    def get_efficientNetV2B0_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return efficientNetV2B0 model and its specifications
        model_name = 'efficientNetV2B0_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2B0(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        # false -> input is in range (-1,1), not (0,255)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetV2B1_model(self, last_layer_count, top_dimen = (240, 240, 3)):
        # Return efficientNetV2B1 model and its specifications
        model_name = 'efficientNetV2B1_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2B1(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
   
    def get_efficientNetV2B2_model(self, last_layer_count, top_dimen = (260, 260, 3)):
        # Return efficientNetV2B2 model and its specifications
        model_name = 'efficientNetV2B2_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2B2(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
        
    def get_efficientNetV2B3_model(self, last_layer_count, top_dimen = (300, 300, 3)):
        # Return efficientNetV2B3 model and its specifications
        model_name = 'efficientNetV2B3_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2B3(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetV2S_model(self, last_layer_count, top_dimen = (384, 384, 3)):
        # Return efficientNetV2S model and its specifications
        model_name = 'efficientNetV2S_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2S(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen

    def get_efficientNetV2M_model(self, last_layer_count, top_dimen = (480, 480, 3)):
        # Return efficientNetV2M model and its specifications
        model_name = 'efficientNetV2M_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2M(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_efficientNetV2L_model(self, last_layer_count, top_dimen = (480, 480, 3)):
        # Return efficientNetV2L model and its specifications
        model_name = 'efficientNetV2L_' + str(last_layer_count)
        model_full = tf.keras.applications.efficientnet_v2.EfficientNetV2L(include_top=True, weights='imagenet', 
                                            input_shape=top_dimen, classifier_activation='linear', include_preprocessing=False )
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    
    # RESNET MODELS ===============================================
    
    def get_resNet50_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return resNet50 model and its specifications
        model_name = 'resNet50_' + str(last_layer_count)
        model_full = tf.keras.applications.resnet50.ResNet50(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen
    
    def get_resNet101_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return resNet101 model and its specifications
        model_name = 'resNet101_' + str(last_layer_count)
        model_full = tf.keras.applications.resnet.ResNet101(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
    
    def get_resNet152_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return resNet152 model and its specifications
        model_name = 'resNet152_' + str(last_layer_count)
        model_full = tf.keras.applications.resnet.ResNet152(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
    
     # INCEPTION MODELS ===============================================
        
    def get_InceptionV3_model(self, last_layer_count, top_dimen = (299, 299, 3)):
        # Return InceptionV3 model and its specifications
        model_name = 'InceptionV3_' + str(last_layer_count)
        model_full = tf.keras.applications.inception_v3.InceptionV3(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
   
    # DENSENET MODELS ===============================================
    
    def get_DenseNet121_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return DenseNet121 model and its specifications
        model_name = 'DenseNet121_' + str(last_layer_count)
        model_full = tf.keras.applications.densenet.DenseNet121(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
    
    def get_DenseNet169_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return DenseNet169 model and its specifications
        model_name = 'DenseNet169_' + str(last_layer_count)
        model_full = tf.keras.applications.densenet.DenseNet169(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
    
    def get_DenseNet201_model(self, last_layer_count, top_dimen = (224, 224, 3)):
        # Return DenseNet201 model and its specifications
        model_name = 'DenseNet201_' + str(last_layer_count)
        model_full = tf.keras.applications.densenet.DenseNet201(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen  
    
    # INCEPTIONRESNET MODELS ===============================================
    
    def get_inceptionResnetV2_model(self, last_layer_count, top_dimen = (299, 299, 3)):
        ## Return inceptionResnetV2 model and its specifications
        model_name = 'inceptionResnetV2_' + str(last_layer_count)
        model_full = tf.keras.applications.inception_resnet_v2.InceptionResNetV2(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        return model, model_name, top_dimen

    # NASNETLARGE MODELS ===============================================
    
    def get_NASNetLarge_model(self, last_layer_count, top_dimen = (331, 331, 3)):
        # Return NASNetLarge model and its specifications
        model_name = 'NASNetLarge_' + str(last_layer_count)
        model_full = tf.keras.applications.nasnet.NASNetLarge(input_shape=top_dimen, include_top=True, weights='imagenet')
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        
        return model, model_name, top_dimen
      
    # XCEPTION MODELS ===============================================
    
    def get_Xception_model(self, last_layer_count, top_dimen = (299, 299, 3)):
        # Return Xception model and its specifications
        model_name = 'Xception_' + str(last_layer_count)
        model_full = tf.keras.applications.xception.Xception(include_top=True, weights='imagenet', input_shape=top_dimen)
        
        model = tf.keras.Model(model_full.input, model_full.layers[-last_layer_count-1].output)
        
        return model, model_name, top_dimen
      

    # GET ALL MODELS ===============================================    
    def get_all_models(self):
        """Return all defined models

        Parameters
        ----------

        Returns
        -------
        array
            Array of all models and its speciications in format [[model, name, inp_dimen], ... ,[model, name, inp_dimen]]
        """
        models = [] 
        
        # Remove 1 last layer
        for i in range(0,2):
            model, model_name, top_dimen = self.get_resNet50_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_resNet101_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_resNet152_model(i)
            models.append([model, model_name, top_dimen])
            
            
            model, model_name, top_dimen = self.get_InceptionV3_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_inceptionResnetV2_model(i)
            models.append([model, model_name, top_dimen])

            
            model, model_name, top_dimen = self.get_DenseNet121_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_DenseNet169_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_DenseNet201_model(i)
            models.append([model, model_name, top_dimen])
            
            
            model, model_name, top_dimen = self.get_NASNetLarge_model(i)
            models.append([model, model_name, top_dimen])
            
            
            model, model_name, top_dimen = self.get_Xception_model(i)
            models.append([model, model_name, top_dimen])
        
        # Remove 2 last layers
        for i in range(0, 3):      
            model, model_name, top_dimen = self.get_efficientNetB0_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB1_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB2_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB3_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB4_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB5_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB6_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetB7_model(i)
            models.append([model, model_name, top_dimen])
            
            
            model, model_name, top_dimen = self.get_efficientNetV2L_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2M_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2S_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2B3_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2B2_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2B1_model(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_efficientNetV2B0_model(i)
            models.append([model, model_name, top_dimen])
            
        # Remove 3 last layers    
        for i in range(0, 4):
            model, model_name, top_dimen = self.get_VGG16_model_only(i)
            models.append([model, model_name, top_dimen])
            
            model, model_name, top_dimen = self.get_VGG19_model_only(i)
            models.append([model, model_name, top_dimen])
            
        return models
