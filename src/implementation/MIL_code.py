import tensorflow as tf
import keras_tuner as kt
import tensorflow.python.ops.math_ops as tf_math_ops
import numpy as np
import pandas as pd

from logbook import Logger
from CONFIG import *
from model_evaluator import ModelEvaluator
from data_helper import DatasetsHandler

np.random.seed(1234)
tf.random.set_seed(1234)

AUTOTUNE = tf.data.experimental.AUTOTUNE

class MILClass():
    def __init__(self, TOP_K_IMGS, MODELS_SAVE_PREFIX, NN, PHASE1_BATCH, PHASE2_BATCH, IMG_SIZE, MIL_VARIANT, MIL_OPERATION, last_layer):
        """Return MIL class with corresponding parameters.

        Parameters
        ----------
        TOP_K_IMGS, MODELS_SAVE_PREFIX, NN, PHASE1_BATCH, PHASE2_BATCH, IMG_SIZE, MIL_VARIANT, MIL_OPERATION, last_layer
        MIL_VARIANT : bool
            True # True = simple MIL, False = ragged MIL
        MIL_OPERATION : str
            Options are 'MAX' # MEAN, MIN, MAX, 
        LAST_LAYER : bool
            True -> Include last layer in 1st DNN. False - don't include it. In case of OUR VGG16: False -> 3072, True -> 1024 
        TOP_K_IMGS : int
            Top-K imgs to consider
        PHASE1_BATCH : int
            Batch size of first phase
        PHASE2_BATCH : int
            Batch size of second phase
        IMG_SIZE : int
            Image size on input (first phase) 224 for VGG16 baseline model, 300 for EfficientNetV2B3, etc. 
        SPLIT_RATIO : float
            Train x Valid split ratio of items with embeddings (train)
        EPOCHS_KT : int
            Kerase tuner epochs

        Returns
        -------
        MILClass
        """
        self.TOP_K_IMGS = TOP_K_IMGS
        self.MODELS_SAVE_PREFIX = MODELS_SAVE_PREFIX
        self.NN = NN 
        self.PHASE1_BATCH = PHASE1_BATCH
        self.PHASE2_BATCH = PHASE2_BATCH
        self.IMG_SIZE = IMG_SIZE
        self.emb_big_lenght = None
        self.MIL_VARIANT = MIL_VARIANT
        self.MIL_OPERATION = MIL_OPERATION
        self.last_layer = last_layer
        self.DH = DatasetsHandler()
        
        self.last_item = None # for reindexing of items
        self.help_c = 0 # for reindexing of items

    # ================= PHASE 1 =====================
    def get_model_phase1(self):
        # Load model for first phase
        #path_to_model = 'models_checkpoint/efficientNetV2B3_2' # ONE_TO_ONE
        path_to_model = 'models_checkpoint/VGG19_3' # ONE_TO_ONE
        #path_to_model = 'models_checkpoint/VGG16_3' # ONE_TO_ONE
        #path_to_model = 'models_checkpoint/two_to_one_VGG16_3' #MANY_TO_ONE
        LR = 0.0000909791 # not important in 1st phase
        model = self.NN.load_my_model_with_custom_loss(path_to_model, LR)
                 
        if self.last_layer: # trim last layer if needed
            return model
        else:
            return tf.keras.Model(model.input, model.layers[-2].output) # trim last prediction layer 

    def phase_1(self, final_merge):
        # predict selected input with first model
        
        # get model
        model_first = self.get_model_phase1()

        # get dataset
        input_data = self.DH.get_tf_ds_without_target(final_merge, self.IMG_SIZE, self.PHASE1_BATCH)

        # get result embeddings_big
        items_embeddings = model_first.predict(input_data) 

        # save results
        final_merge['embeddings_big'] = items_embeddings.tolist()
       
        # save lenght of resulting embedding 
        self.emb_big_lenght = len(final_merge['embeddings_big'].iloc[0]) # lenght of big embedding
        
        #return final_merge
        if self.MIL_VARIANT:
            # SIMPLE VARIANT we need few more steps to join 2+ images into 1       
            # self.VATIANT = MEAN/MAX/MIN
            final_merge = self.calculate_mean_of_big_embedd_phase2(final_merge, self.emb_big_lenght, self.MIL_OPERATION) 
        
        return final_merge    
    
    def predict_datasets_phase1(self, final_merge, pics_paths_unique):
        # Predict datsets on phase 1 (i.e., create embeddings from images, train and valid)
        # training dataset
        final_merge = self.DH.filter_top_K_imgs(final_merge, self.TOP_K_IMGS) # filter top K images
        final_merge_ALS = self.phase_1(final_merge) # get prediction

        # evaluation dataset (no embeddings)
        pics_paths_unique = self.DH.make_predict_dataset(pics_paths_unique, self.TOP_K_IMGS) # filter correct dataset
        pics_paths_predict = self.phase_1(pics_paths_unique) # get prediction
        
        return final_merge_ALS, pics_paths_predict
    
    def phase1_get_all_datasets(self, final_merge, pics_paths_unique, SPLIT_RATIO):
        """Get datasets needed from phase 1 -> for train (with ALS embeddings) and without them.

        Parameters
        ----------
        final_merge : pandas.DataFrame
            Dataframe with path to images and ALS embedding.
        pics_paths_unique : pandas.DataFrame
            Dataframe without path to images (for evaluation).
        SPLIT_RATIO : float
            Ratio between train & valid datasets.

        Returns
        -------
        (pandas.DataFrame, pandas.DataFrame, [np.array, pandas.DataFrame])
            Train, valid tf.data.Datasets with ALS embeddings as target
            in [] is predicted embeddings of images in following dataframe (for evaluation)
            
        """
        # get train and evaluation datasets
        ALS, predict_embed = self.predict_datasets_phase1(final_merge, pics_paths_unique)
        
        # split ALS dataset
        ALS_train, ALS_valid = self.DH.splitter(ALS, SPLIT_RATIO)
        
        # convert into tf.Dataset
        if self.MIL_VARIANT:
            train_train_ds = self.DH.get_dataset_simple_phase2(ALS_train, self.PHASE2_BATCH, self.IMG_SIZE, False)
            train_valid_ds = self.DH.get_dataset_simple_phase2(ALS_valid, self.PHASE2_BATCH, self.IMG_SIZE, False)
            predict_embed_ds = self.DH.get_dataset_simple_phase2(predict_embed, self.PHASE2_BATCH, self.IMG_SIZE, True) # and create tf.Dataset for phase2        
        else:
            # RAGGED
            train_train_ds = self.ragged_NN_only_ds_phase2(ALS_train) # training dataset
            train_valid_ds = self.ragged_NN_only_ds_phase2(ALS_valid) # Valid dataset
            
            predict_embed_ds = self.tensor_from_ALS_phase2(predict_embed) # create dataset 
            predict_embed = predict_embed.groupby(['item_id']).first().reset_index()#.set_index('index')
            
        return train_train_ds, train_valid_ds, [predict_embed, predict_embed_ds]
    # ================= PHASE 2 =====================
    # -------------- PHASE 2 SIMPLE --------------------------------
    def calculate_mean_of_big_embedd_phase2(self, ALS, emb_big_lenght, MIL_OPERATION):
        """Perform MIL operation (SIMPLE MIL) and return only
           one embedding of each item to one ALS embedding.

        Parameters
        ----------
        ALS : pandas.DataFrame
            Dataframe with predicted embeddings.
        emb_big_lenght : int
            Lenght of embedding generated in phase 1
        MIL_OPERATION : str
            Name of MIL operation

        Returns
        -------
        pandas.DataFrame
            Dataframe with joint embeddings into one.
            
        """
        # calculate mean of big_embeddings for each item

        ALS_max = ALS.groupby(['item_id'], sort=False)['image_number'].max() # take max items 
        images_cnt_per_items = ALS_max.to_numpy(dtype=np.int32).flatten() + 1 # add one (starting from 0)

        image_embeddings_queue = np.vstack(ALS['embeddings_big'])

        end_indexes = np.cumsum(images_cnt_per_items)
        start_indexes = np.concatenate(([0], np.cumsum(images_cnt_per_items)[:-1]))

        embedd_np = np.zeros(shape=(len(ALS), emb_big_lenght)) # prepare np array
        for start, end in zip(start_indexes, end_indexes):
            embeddings = image_embeddings_queue[start:end]

            emb_np = np.array(embeddings, dtype=np.float32)

            if MIL_OPERATION == 'MEAN':
                new_embeddings = np.mean(emb_np, axis=0) # get mean of embeddings_big
            elif MIL_OPERATION == 'MIN':
                new_embeddings = np.min(emb_np, axis=0) # get min of embeddings_big
            elif MIL_OPERATION == 'MAX':
                new_embeddings = np.max(emb_np, axis=0) # get max of embeddings_big
            else: 
                print("No variant selected. We will do MEAN")
                new_embeddings = np.mean(emb_np, axis=0) # get mean of embeddings_big
                

            if end < len(embedd_np):
                new_embeddings = np.tile(new_embeddings, (end-start, 1)) # connext X times this mean/min/max
                embedd_np[start:end] = new_embeddings
            else:
                #print(start, end, "SADJASLDKJASLJ")
                new_embeddings = np.tile(new_embeddings, (1, 1)) # connext X times this mean/min/max
                embedd_np[start:start+1] = new_embeddings


        # connect it to orignal dataset
        ALS['embedd_simple']=embedd_np.tolist()
        ALS_mean = ALS[ALS['image_number'] == 0] # we need only 1 mean embedd & 1 embedding
        ALS_mean = ALS_mean.reset_index()
        return ALS_mean

    # -------------- PHASE 2 RAGGED --------------------------------
    def ragged_NN_only_ds_phase2(self, ALS):
         # ALS -> tensor with corresponding dimensions for MIL (x, TOP_K_IMGS, emb_len)
        embedd_tensor = self.tensor_from_ALS_phase2(ALS)

        # Target embeddings
        target_als = np.array(ALS.groupby('item_id').first()['embedding'].tolist() ,dtype=np.float32)

        # Create TF.data.dataset
        ds = self.make_tf_dataset_phase2(embedd_tensor, target_als, self.PHASE2_BATCH)
        return ds
    
    def make_tf_dataset_phase2(self, x,y, BATCH_SIZE=32):
        # get target ALS embeddings + tf.data.dataset
        ds = tf.data.Dataset.from_tensor_slices((x,y))
        ds = ds.batch(BATCH_SIZE)
        ds = ds.prefetch(buffer_size=AUTOTUNE)
        return ds

    def tensor_from_ALS_phase2(self, ALS):
        # Make Data ready for TF 
        # transform dict into np array of (x, TOP_K_IMGS, emb_len)
        # ,where x = # of unique items, K-items, embedding from phase 1 lenght

        ALS_max_items = ALS.assign(max_items=np.nan) # add new column 'max_items' with NaN values
        ALS_max_items = ALS_max_items.groupby('item_id').aggregate(max_items=('image_number', max))
        images_cnt_per_items = ALS_max_items.to_numpy(dtype=np.int32).flatten() + 1
        
        images_cnt_per_items = tf.convert_to_tensor(images_cnt_per_items)

        big_embed = np.vstack(ALS['embeddings_big'])
        image_embeddings_queue = tf.convert_to_tensor(big_embed)

        start_indexes = tf.math.cumsum(images_cnt_per_items, exclusive=True)
        end_indexes = tf.math.cumsum(images_cnt_per_items)

        embedd_tensor = None 
        for start, end in zip(start_indexes, end_indexes):
            embeddings = image_embeddings_queue[start:end]
            if embeddings.shape[0] < self.TOP_K_IMGS:
                new_embeddings = tf.concat([embeddings, tf.repeat(tf.zeros_like(image_embeddings_queue[0])[tf.newaxis, :],
                                                                  self.TOP_K_IMGS - embeddings.shape[0], axis=0)], axis=0)
            else:
                new_embeddings = embeddings

            if embedd_tensor is None:
                embedd_tensor = new_embeddings[tf.newaxis, :]
            else:
                embedd_tensor = tf.concat([embedd_tensor, new_embeddings[tf.newaxis, :]],axis=0)
     
        return embedd_tensor
    
    # TUNERs FOR PHASE 2 =====================================
    def get_model_tuner_RAGGED(self, hp):
        # Definition of hyperparameters for Ragged MIL
        
        hp_ragged = hp.Choice('ragged_output', values=[1024, 1536, 2048, 3072])
        main_model = MILRagged(self.TOP_K_IMGS, hp_ragged, self.MIL_OPERATION)
        model_name = 'RAGGED_MIL'
        
        # Define end layers
        prediction_layer = tf.keras.layers.Dense(1024, activation='linear', name='predictions')
        model = tf.keras.Sequential([
                main_model,
                ], name=model_name)
       
        # dense_layers --------------------
        hp_dense = hp.Choice('dense_layers', values=[0, 1, 2, 3])
        hp_N_multiple = hp.Choice('dense_multiple', values=[1, 2, 3])
        
        for i in range(hp_dense):
            model.add(tf.keras.layers.Dense(1024*hp_N_multiple, activation='relu', name='fc' + str(i)))
        model.add(prediction_layer)

        # optimziers + LR --------------------
        hp_LR = hp.Float('learning_rate', min_value=1e-5, max_value=1e-2, sampling='LOG')
        optimizer = tf.keras.optimizers.Adam(learning_rate=hp_LR)
        
        self.NN.compile_model(model, optimizer)
                
        return model
    
    def get_model_tuner_SIMPLE(self, hp):
        # Definition of hyperparameters for Simple MIL
        model_name = 'SIMPLE_MIL'
        
        # Define end layers
        prediction_layer = tf.keras.layers.Dense(1024, activation='linear', name='predictions')
        model = tf.keras.Sequential([
                ], name=model_name)
       
        # dense_layers --------------------
        hp_dense = hp.Choice('dense_layers', values=[0, 1, 2, 3])
        hp_N_multiple = hp.Choice('dense_multiple', values=[1, 2, 3])
        
        for i in range(hp_dense):
            model.add(tf.keras.layers.Dense(1024*hp_N_multiple, activation='relu', name='fc' + str(i)))
        model.add(prediction_layer)

        # optimziers + LR --------------------
        hp_LR = hp.Float('learning_rate', min_value=1e-5, max_value=1e-2, sampling='LOG')
        optimizer = tf.keras.optimizers.Adam(learning_rate=hp_LR)
        
        self.NN.compile_model(model, optimizer)
                
        return model
    
    
class MILRagged(tf.keras.Model):
    # Code inspisred from Vojtech Vancura
    def __init__(self, nr_of_items, output_size, MIL_OPERATION):
        super(MILRagged, self).__init__()
        # Creates Ragged MIL with needed operation(s). 
        # Change code if need someting else than ONLY min/max/mean.
        
        self.MIL_OPERATION = MIL_OPERATION
        
        if self.MIL_OPERATION == 'MIN':
            self.i1 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Min_pool")
        elif self.MIL_OPERATION == 'MEAN':
            self.i2 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Mean_pool")
        elif self.MIL_OPERATION == 'MAX':
            self.i3 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Max_pool")
        else:
            self.i1 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Min_pool")
            self.i2 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Mean_pool")
            self.i3 = tf.keras.layers.Dense(output_size, activation='relu', input_shape=(nr_of_items,), name="Max_pool")
            
    @tf.function
    def pooling(self, x):
        a = tf.math.count_nonzero(x, axis=2)   # Count how many items/embeddings do I have in this batch
        b = tf.math.reduce_any(tf_math_ops.not_equal(x, 0), axis=2)   # Change dimension of the tensor from
        
        count_items = tf.keras.backend.sum(a, axis=1)
        count_items = tf.cast(count_items, 'float32')
        count_items = tf.expand_dims(count_items, axis=1)

        # r1 is ragged tensor and consists of data transformed by Min_pool layer and then masked to remove zeros
        if self.MIL_OPERATION == 'MIN':
            r1 = tf.ragged.boolean_mask(self.i1(x), b)
            mini = tf.keras.backend.min(r1, axis=1)  # Mini is a normal tensor, not ragged
            c = tf.concat([count_items, mini], axis=1)
        elif self.MIL_OPERATION == 'MEAN':
            r2 = tf.ragged.boolean_mask(self.i2(x), b)
            mean = tf.keras.backend.mean(r2, axis=1)
            c = tf.concat([count_items, mean], axis=1)
        elif self.MIL_OPERATION == 'MAX':
            r3 = tf.ragged.boolean_mask(self.i3(x), b)
            maxi = tf.keras.backend.max(r3, axis=1)
            c = tf.concat([count_items, maxi], axis=1)
        else:
            r1 = tf.ragged.boolean_mask(self.i1(x), b)
            r2 = tf.ragged.boolean_mask(self.i2(x), b)
            r3 = tf.ragged.boolean_mask(self.i3(x), b)
            mini = tf.keras.backend.min(r1, axis=1) 
            mean = tf.keras.backend.mean(r2, axis=1)
            maxi = tf.keras.backend.max(r3, axis=1)

   
        # For those variant put self.VARIANT != ['MIN', 'MAX', 'MEAN'] and (un)comment corresponding code
        #c = tf.concat([count_items, mini, mean, maxi], axis=1)
        #c = tf.concat([count_items, mean, maxi], axis=1)
        #c = tf.concat([count_items, mini, maxi], axis=1)
        #c = tf.concat([count_items, mini, mean], axis=1)
        #c = tf.concat([count_items, maxi, maxi], axis=1)
        
        return c

    @tf.function
    def call(self, x):
        # ex: consider batch of 100 samples with basket of max 4 items, each item is represented by 4537 vector
        # this mean x has shape (100, 4, 4537)  -> 4 je MAX items -> muzu experimentovat kolik je vhodne, treba 5 max
        # (batch size, max # of obrazku, dimense embeddingy)
        
        #print('input shape:', x.shape)
        # first, apply pooling op to all examples in batch
        c = self.pooling(x)
        #print('output shape:', c.shape)
       
        # in total we transformed 1 batch of 100 samples: (100, 4, 4537) --> (100, 3x4537 +1 )
        return c
