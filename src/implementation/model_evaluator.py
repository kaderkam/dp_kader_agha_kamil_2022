import os
import time
import json
import numpy as np
from logbook import Logger
import logbook

from sklearn.preprocessing import normalize
from sklearn.neighbors import NearestNeighbors
import keras_tuner as kt
import tensorflow as tf
import matplotlib.pyplot as plt

from CONFIG import *
from my_timer import timeit
from RecallMeter import RecallMeter

np.random.seed(1234)


class ModelEvaluator:
    def __init__(self, path_for_logs):
        """Prepare ModelEvaluator class

        Parameters
        ----------
        path_for_logs : str
            Path where to save logs.
            
        Returns
        -------
        ModelEvaluator
        """
        self.my_logger = Logger('My Logger') # Logger for loging
        self.handler = logbook.FileHandler(path_for_logs, mode='a+',) # handler for saving logs into file
        self.log_time = {}

    @timeit
    def predict_all_items(self, model, predict_embed_ds, log_time):
        # get predicitons of 'predict_embed_ds' with model 'model'.
        feature_vector = model.predict(predict_embed_ds)
        return feature_vector

    @timeit
    def compute_similar_items(self, compute_embed, K, log_time):
        """Compute KNN of items with embedding
        Inspired by: https://stackoverflow.com/questions/34144632/
                     using-cosine-distance-with-scikit-learn-kneighborsclassifier

        Parameters
        ----------
        compute_embed : pandas.DataFrame
            Dataframe with 'embeddings', 'item_id' columns.
        K : int
            Number of KNN.
        log_time : dict
            Dict, where will be saved time needed of this function as ('fce_name': time)
            
        Returns
        -------
        dict
            Dictionary of arrays of K - nearest neighbors. 
            Example of K = 2 {'item1' : ['item2', 'item3',], 'item2' : ['item1', 'item3'],...]
        """
        X = np.array(compute_embed['embeddings'].tolist())
        X = normalize(X, axis=1, norm='l2')

        # Get neighbors of items.
        nbrs = NearestNeighbors(n_neighbors=K+1, algorithm='brute', metric='cosine').fit(X)
        cos_distances, indices = nbrs.kneighbors(X)

        cosine_similarity = 1-cos_distances # make cosine similarity from cosine distance.
        similar_items = {} # resulting dict
        for i in range(len(indices)):
            i_name = compute_embed.iloc[indices[i][0]]['item_id']
            similar_items[i_name] = []
            for j in range(1, len(indices[i])):
                i_name_other = compute_embed.iloc[indices[i][j]]['item_id']
                similar_items[i_name].append((i_name_other, cosine_similarity[i][j] ))

        return similar_items

    @timeit
    def evaluate_embedding(self, similar_items, raw_interactions, my_logger, log_time):
        # LOOCV recall calculation
        RM = RecallMeter(my_logger)
        return RM.measure_leave_one_out(similar_items, raw_interactions, {})
    
    def get_my_callbacks(self, save_path):
        # return callbacks (ModelCheckpoint & EarlyStopping)
        checkpoint = tf.keras.callbacks.ModelCheckpoint(MODELS_SAVE_PREFIX + save_path, monitor="val_loss",
                                                        verbose=1, 
                                                        save_best_only=True, save_weights_only = False, mode="min", 
                                                        save_freq="epoch", options=None)

        early_stop = tf.keras.callbacks.EarlyStopping(monitor="val_loss", min_delta=0, patience=3, 
                                                      verbose=1, mode="min", restore_best_weights="True")

        return [early_stop, checkpoint]

    @timeit
    def train_model(self, model, train_ds, valid_ds, save_path, epoch_total, **kwargs):
        # Train model on train&valid datasets, with callbacks. Save best models in save_path.
        history = model.fit(train_ds,
                            epochs=epoch_total,
                            steps_per_epoch = None,
                            validation_steps = None,
                            validation_data = valid_ds, 
                            callbacks=self.get_my_callbacks(save_path))
        return history
    
    def pre_train_model(self, model, train_ds, valid_ds, save_path, epoch_total):
        # train model & log about time needed
        print('Training model...')
        start = time.time()
        with self.handler:
            history = self.train_model(model, train_ds, valid_ds, save_path, epoch_total, log_time=self.log_time)
        end = time.time()
        print('Training end: ', str(end - start) + 's')
        self.my_logger.info('Training end: ' + str(end - start) + 's')
        
        return history

    
    def predict_embeddings(self, model, predict_embed_wrap):
        # predict all items (create thier embeddings) on model
        print("Predicting all items starting...")
        start = time.time()
        with self.handler:
            items_embeddings = self.predict_all_items(model, predict_embed_wrap[1], log_time=self.log_time)
            items_embeddings = np.nan_to_num(items_embeddings) # sometimes it get nan/+-inf when model is really bad
        predict_embed_wrap[0]['embeddings'] = items_embeddings.tolist()
        end = time.time()
        print('Predict end: ', str(end - start) + 's')
        self.my_logger.info('Predict end: ' + str(end - start) + 's')
    
        return predict_embed_wrap[0], items_embeddings
    
    def find_KNN_items(self, compute_embed):
        # Find KNN & log about time needed
        # OUTPUT: similar_items = {'item_id_1' : [(item_id_2, 0.78), (item_id_3, 0.77), ...]}
        print("computing similar items...")
        start = time.time()
        with self.handler:
            similar_items = self.compute_similar_items(compute_embed, 5, log_time=self.log_time)
        end = time.time()
        print('Compute end: ', str(end - start) + 's')
        self.my_logger.info('Compute end: ' + str(end - start) + 's')
        return similar_items
        
    def evaluate_KNN(self, similar_items, raw_interactions):
        # Compute LOOCV & log about time needed
        print("evaluating embedding...")
        start = time.time()
        with self.handler:
            res = self.evaluate_embedding(similar_items, raw_interactions, self.my_logger, log_time=self.log_time)
        end = time.time()
        print('Evaluate end: ', str(end - start) + 's')
        self.my_logger.info('Evaluate end: ' + str(end - start) + 's')
        return res
    
    def train_eval_model(self, model_wrapper, datasets, predict_embed_wrap, raw_interactions):
        """Train (optional) and evaluate model on given datasets.
        
        Parameters
        ----------
        model_wrapper : array
            In format: [epoch_count, model, model_name] - number of epochs, model, model name
        datasets : array
            Array of train and valid datasets. [traind_ds, valid_ds] in tf.data.Datatset format.
        predict_embed_wrap : array
            In format: [pics_paths_unique, predict_embed_ds] - dataframe of evaluation images and 
            the same dataframe converted into tf.data.Dataset, ready for prediction in TensorFlow.
        raw_interactions : pandas.DataFrame
            Dataframe of interactions, ready to be used in LOOCV method. 
            
        Returns
        -------
        pandas.DataFrame
            Dataframe with columns 'k' (int), 'beta' (float), 'recall' (float) and 'recommended-items' (int).
        """
        tried_combinations = self.load_tried_combinations() # logs for trained networks
        model_name = model_wrapper[2] # unwrapp model name
        
        # 1) TRAIN & PREDICT
        if model_wrapper[0]: # Train model if needed
            # INPUT: model, train & valid datasets, save_path (where to save model), number of epochs
            history = self.pre_train_model(model_wrapper[1], datasets[0], datasets[1], model_name, model_wrapper[0])
            
            self.make_graph_of_fit_history(history, model_name) # create graph history of learning

        model = model_wrapper[1] # unwrap model

        # 2) Get predictions from our model
        compute_embed, items_embeddings = self.predict_embeddings(model, predict_embed_wrap)
    
        # 3) Compute similar Items
        similar_items = self.find_KNN_items(compute_embed)

        # 4) Evaluate KNN (compute recall)
        results = self.evaluate_KNN(similar_items, raw_interactions)
        
        # Log results
        self.my_logger.info(model_name + ': ' + str(self.log_time))
        self.my_logger.info("------")
        self.my_logger.info(results)
        self.my_logger.info("------")  
        self.save_tried_combinations(tried_combinations, model_name, results) # save results of this model

        return results
    
    def make_graph_of_fit_history(self, history, model_name):
        # Draw graph of 'loss' and 'val_loss'
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model history')
        plt.ylabel('COS/MSE value')
        plt.xlabel('epoch')
        plt.legend(['loss', 'val loss'], loc='upper left')
        plt.savefig('PLOT_PICS/' + str(model_name) + '.png')
        plt.show() 
        
    # SAVE ONLY RESULTS INTO FILE ========================= 
    def save_tried_combinations(self, tried_combinations, model_name, res):
        # Save results (recall) in file in JSON representing dictionary,
        # with results of 'train_eval_model' function.
        
        if model_name not in tried_combinations:
            tried_combinations[model_name] = [] # if first time of model, make his dict
            
        res_agg = []
        for i in range(4):
            res_agg.append([i, float(res['recommended-items'].iloc[i]), float(res['recall'].iloc[i])])
        res_agg.append("====")
        tried_combinations[model_name].append(res_agg) # append new result.

        # write it on disk
        open_path = SAVE_MODEL_RESULTS
        with open(open_path, "w") as outfile:
            json.dump(tried_combinations, outfile)

    def load_tried_combinations(self):
        # Load from disk history of results (recalls)
         
        open_path = SAVE_MODEL_RESULTS
        f = open(open_path, 'w+')

        # If not fileor is empty, create new dict
        if os.stat(open_path).st_size == 0: 
            f.close()
            return dict()

         data = json.load(f)

        f.close()
        
        if data is None or len(data) == 0:
            return dict()
        return data
    
