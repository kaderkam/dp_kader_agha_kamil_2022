# This code is from work of my supervisor Petr Kasalicky

import re
from collections import Counter

import numpy as np
import pandas as pd
try:
    PANDARALLEL_IMPORTED = True
    from pandarallel import pandarallel
except ImportError:
    PANDARALLEL_IMPORTED = False

import logbook
import time

class Timer:
    def __init__(self, name, logger):
        self.name = name
        self.logger = logger
        self.t0 = None
    def __enter__(self):
        self.t0 = time.time()
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.logger.debug('%s: %s seconds' % (self.name, time.time()-self.t0))

class RecallMeter:
    """
    Measure popularity-stratified user-normalized leave-(last-)one-out recall.
    """

    def __init__(self, logger: logbook.Logger, seed: int = None, n_threads: int = None):
        """
        Create instance of RecallMeter with given logger, seed and number of threads to use.

        Parameters
        ----------
        logger : logbook.Logger
            Instance of Logger to use for logging.
        seed : int, default: None
            Seed of the generator for random choices.
        n_threads : int, default: None
            Number of threads to use for measuring recall.
            If not specified, use all threads available.
        """
        self.__logger = logger
        if seed is not None:
            self.__logger.info("Seed for random selection of users was manually set to {}".format(seed))
            np.random.seed(seed)
        self.__n_threads = int(n_threads) if n_threads is not None else None
        if PANDARALLEL_IMPORTED:
            if self.__n_threads is None:
                pandarallel.initialize()
            elif self.__n_threads == 1:
                self.__logger.info("Number of threads was manually set to {}, therefore there is no need to initialize pandarallel".format(self.__n_threads))
            else:
                self.__logger.info("Number of threads for pandarallel was manually set to {}".format(self.__n_threads))
                pandarallel.initialize(nb_workers=self.__n_threads)
        else:
            self.__logger.info("Pandarallel is not instaled, ignoring number of threads parameter set to {}".format(self.__n_threads))

    def measure_leave_one_out(self, similar_items: dict, interactions: pd.DataFrame, items_popularity: dict,
                              k: list = [5, 25, 50, 100], beta: list = [0], min_items_per_user: int = 2, max_items_per_user: int = None,
                              measure_for_max_n_users: int = None) -> pd.DataFrame:
        """
        Measure popularity-stratified user-normalized leave-one-out recall for cartesian product of 'k' values and 'beta' values.

        Parameters
        ----------
        similar_items : dict
            Dictionary where key is 'item_id' and value is list of nearest neighbors along with their similarity.
            Example: 'item_id_1' [(item_id_2, 0.78), (item_id_3, 0.77), ...].
        interactions : pandas.DataFrame
            Interactions used as test interactions for measuring recall.
            Pandas dataframe with columns 'user_id', 'item_id', 'value' where each tuple (user_id, item_id) is unique.
        items_popularity : dict
            Dictionary with popularities of item, key is item_id, value is float.
        k : list or int, default: [5]
            Int or list of ints used defined for Top-k recommendation.
        beta : list of float, default: [0]
            Float of list of floats determining penalization of popular items according to popular-stratified recall.
        min_items_per_user : int, default: 2
            Minimal number of items interacted by an user to use its interactions for measuring.
        max_items_per_user : int, default: None
            Maximum number of items interacted by an user to use its interactions for measuring.
        measure_for_max_n_users : int, default: None
            Maximum number of used test users.
            If not defined, use all users in interactions dataframe.
            If defined, select users randomly (or using a seed) from the set of all users.

        Returns
        -------
        pandas.DataFrame
            Dataframe with columns 'k' (int), 'beta' (float), 'recall' (float) and 'recommended-items' (int).
        """
        if min_items_per_user < 2:
            raise ValueError("Cannot measure recall for users with less then 2 interacted items, \
                but value min_items_per_user={} was given. ".format(min_items_per_user))

        if isinstance(k, list):
            ks = [int(i) for i in k]
        else:
            ks = [int(k)]

        if isinstance(beta, list):
            betas = [float(b) for b in beta]

        else:
            betas = [float(beta)]

        regex = re.compile("=([0-9.]+)")

        if max_items_per_user is None:
            max_items_per_user = np.inf

        number_of_users_before_filtering = interactions['user_id'].nunique()
        with Timer("Users ({}) were filtered to have number of interacted items in range [{}, {}]".format(
                   number_of_users_before_filtering, min_items_per_user, max_items_per_user), self.__logger):
            interactions = interactions.groupby('user_id').filter(lambda x: min_items_per_user <= len(x) <= max_items_per_user)
            interactions['item_id-value'] = list(zip(interactions['item_id'], interactions['value']))

        unique_users = interactions['user_id'].values.unique()
        if measure_for_max_n_users is not None and len(unique_users) > measure_for_max_n_users:
            with Timer("Randomly selected {} users from all {} users".format(measure_for_max_n_users, len(unique_users)), self.__logger):
                interactions = interactions[interactions['user_id'].isin(
                    np.random.choice(unique_users, replace=False, size=measure_for_max_n_users))
                ]
            number_of_users_after_filtering = measure_for_max_n_users
        else:
            number_of_users_after_filtering = len(unique_users)

        with Timer("Weights of the users ({}) according to the popularity of interacted items were calculated".format(
                   number_of_users_after_filtering), self.__logger):
            interactions['item-popularity'] = interactions['item_id'].map(items_popularity).fillna(1)
            beta_columns = []
            for b in betas:
                column_name = 'beta={}'.format(b)
                interactions[column_name] = np.power(interactions['item-popularity'], -b)
                interactions[column_name] = interactions[column_name] / interactions[column_name].sum()
                beta_columns.append(column_name)

        with Timer("Users ({}) were grouped to each get a list of item IDs".format(number_of_users_after_filtering), self.__logger):
            agg_params = {**{'item_id-value': list}, **{name: np.sum for name in beta_columns}}
            items_per_user = interactions.groupby('user_id', observed=True).agg(agg_params)

        def measure_recall_for_user(items_for_user: list):
            hits = {k: [] for k in ks}
            popularities_for_items = []
            recommended_items = {k: set() for k in ks}
            for (item_id_hidden, _) in items_for_user:
                popularities_for_items.append(items_popularity.get(item_id_hidden, 1))
                topK = RecallMeter.item_knn(
                    interactions=[(value, item_id) for (item_id, value) in items_for_user if item_id != item_id_hidden],
                    similar_items=similar_items,
                    k=max(ks)
                )
                for k in ks:
                    recommendation = set(i for i, _ in topK[:k])
                    recommended_items[k].update(recommendation)
                    hits[k].append(item_id_hidden in recommendation)
            items_popularity_with_beta = {
                b: np.power(popularities_for_items, -b)
                for b in betas
            }
            res = {}
            for k in ks:
                hits_np = np.array(hits[k])
                for b in betas:
                    res["recall k={}, beta={}".format(k, b)] = (items_popularity_with_beta[b][hits_np].sum() / items_popularity_with_beta[b].sum())
                res["recommended-items-k={}".format(k)] = recommended_items[k]
            return pd.Series(res)

        self.__logger.info("Measuring recall...")

        with Timer("Recall and list of recommended items was measured for {} users".format(number_of_users_after_filtering), self.__logger):
            if (self.__n_threads is None or self.__n_threads > 1) and PANDARALLEL_IMPORTED:
                items_per_user = items_per_user.sample(frac=1)
                hits_ratios = items_per_user['item_id-value'].parallel_apply(measure_recall_for_user)
            else:
                hits_ratios = items_per_user['item_id-value'].apply(measure_recall_for_user)

            recall_columns = [column for column in hits_ratios.columns if "recall" in column]
            coverage_columns = [column for column in hits_ratios.columns if "recommended-items" in column]
            recommended_items_per_k = hits_ratios.groupby(lambda _: True).agg({
                c: lambda x: set.union(*x)
                for c in coverage_columns
            })
            recommended_items_per_k = {regex.findall(c)[0]: len(recommended_items_per_k[c].iloc[0]) for c in coverage_columns}

        hits_ratios = hits_ratios.merge(items_per_user, left_index=True, right_index=True)
        results = []
        for recall_column in recall_columns:
            for beta_column in beta_columns:
                k, beta = regex.findall(recall_column)
                if beta_column in recall_column and beta in regex.findall(beta_column):
                    results.append({
                        "k": k,
                        "beta": beta,
                        "recommended-items": recommended_items_per_k[k],
                        "recall": (hits_ratios[recall_column] * hits_ratios[beta_column]).sum()
                    })
        return pd.DataFrame.from_dict(results)

    def measure_leave_last_one_out(self, similar_items: dict, interactions: pd.DataFrame, items_popularity: dict,
                                   k: list = [5], beta: list = [0], min_interactions: int = 3, recomm_last_n_interactions: int = 1,
                                   measure_for_max_n_users: int = None) -> pd.DataFrame:
        """
        Measure popularity-stratified user-normalized leave-last-one-out recall for cartesian product of 'k' values and 'beta' values.

        Parameters
        ----------
        similar_items : dict
            Dictionary where key is 'item_id' and value is list of nearest neighbors along with their similarity.
            Example: 'item_id_1' [(item_id_2, 0.78), (item_id_3, 0.77), ...].
        interactions : pandas.DataFrame
            Interactions used as test interactions for measuring recall.
            Pandas dataframe with columns 'user_id', 'item_id', 'value', 'timestamp'.
        items_popularity : dict
            Dictionary with popularities of item, key is item_id, value is float.
        k : list or int, default: [5]
            Int or list of ints used defined for Top-k recommendation.
        beta : list of float, default: [0]
            Float of list of floats determining penalization of popular items according to popular-stratified recall.
        min_interactions : int, default: 3
            Minimal number of interaction used for recommendation.
        recomm_last_n_interactions : int, default: 1
            For each user, create 'recomm_last_n_interactions' sequences to recommend following interaction.
            Default behavior is to use all interaction except the last one and try to recommend to the last.
        measure_for_max_n_users : int, default: None
            Maximum number of used test users.
            If not defined, use all users in interactions dataframe.
            If defined, select users randomly (or using a seed) from the set of all users.

        Returns
        -------
        pandas.DataFrame
            Dataframe with columns 'k' (int), 'beta' (float), 'recall' (float) and 'recommended-items' (int).
        """
        if isinstance(k, list):
            ks = [int(i) for i in k]
        else:
            ks = [int(k)]
        if isinstance(beta, list):
            betas = [float(b) for b in beta]
        else:
            betas = [float(beta)]

        regex = re.compile("=([0-9.]+)")
        number_of_users_before_filtering = interactions['user_id'].nunique()
        if min_interactions is not None:
            with Timer("Users ({}) were filtered to have at least {} interactions since minimal support for recommendation is {} interactions".format(
                       number_of_users_before_filtering, min_interactions + 1, min_interactions), self.__logger):
                interactions = interactions.groupby('user_id').filter(lambda x: min_interactions < len(x))
        interactions['item_id-value-timestamp'] = list(zip(interactions['item_id'], interactions['value'], interactions['timestamp']))
        unique_users = interactions['user_id'].unique()
        if measure_for_max_n_users is not None and len(unique_users) > measure_for_max_n_users:
            with Timer("Randomly selected {} users from all {} users".format(measure_for_max_n_users, len(unique_users)), self.__logger):
                interactions = interactions[interactions['user_id'].isin(
                    np.random.choice(unique_users, replace=False, size=measure_for_max_n_users))
                ]
            number_of_users_after_filtering = measure_for_max_n_users
        else:
            number_of_users_after_filtering = len(unique_users)

        with Timer("Weights of the users ({}) according to the popularity of interacted items were calculated".format(
                   number_of_users_after_filtering), self.__logger):
            interactions['item-popularity'] = interactions['item_id'].map(items_popularity).fillna(1)
            beta_columns = []
            for b in betas:
                column_name = 'beta={}'.format(b)
                interactions[column_name] = np.power(interactions['item-popularity'], -b)
                interactions[column_name] = interactions[column_name] / interactions[column_name].sum()
                beta_columns.append(column_name)

        with Timer("Users ({}) were grouped to each get a list of item IDs".format(number_of_users_after_filtering), self.__logger):
            agg_params = {**{'item_id-value-timestamp': list}, **{name: np.sum for name in beta_columns}}
            items_per_user = interactions.groupby('user_id', observed=True).agg(agg_params)

        def measure_recall_for_user(items_for_user: list):
            hits = {k: [] for k in ks}
            popularities_for_items = []
            recommended_items = {k: set() for k in ks}
            for i in range(1, recomm_last_n_interactions + 1):
                support = items_for_user[:-i]
                if abs(i) >= len(items_for_user):
                    continue
                to_recommend = items_for_user[-i]
                if len(support) < min_interactions:
                    continue
                popularities_for_items.append(items_popularity.get(to_recommend[0], 1))
                topK = RecallMeter.item_knn(
                    interactions=[(value, item_id) for item_id, value, timestamp in support],
                    similar_items=similar_items,
                    k=max(ks)
                )
                for k in ks:
                    recommendation = set(i for i, _ in topK[:k])
                    recommended_items[k].update(recommendation)
                    hits[k].append(to_recommend[0] in recommendation)

            items_popularity_with_beta = {
                b: np.power(popularities_for_items, -b)
                for b in betas
            }
            res = {}
            for k in ks:
                hits_np = np.array(hits[k])
                for b in betas:
                    res["recall k={}, beta={}".format(k, b)] = (items_popularity_with_beta[b][hits_np].sum() / items_popularity_with_beta[b].sum())
                res["recommended-items-k={}".format(k)] = recommended_items[k]
            return pd.Series(res)

        self.__logger.info("Measuring recall...")

        with Timer("Recall and list of recommended items was measured for {} users".format(number_of_users_after_filtering), self.__logger):
            if (self.__n_threads is None or self.__n_threads > 1) and PANDARALLEL_IMPORTED:
                items_per_user = items_per_user.sample(frac=1)
                hits_ratios = items_per_user['item_id-value-timestamp'].parallel_apply(measure_recall_for_user)
            else:
                hits_ratios = items_per_user['item_id-value-timestamp'].apply(measure_recall_for_user)

            recall_columns = [column for column in hits_ratios.columns if "recall" in column]
            coverage_columns = [column for column in hits_ratios.columns if "recommended-items" in column]
            recommended_items_per_k = hits_ratios.groupby(lambda _: True).agg({
                c: lambda x: set.union(*x)
                for c in coverage_columns
            })
            recommended_items_per_k = {regex.findall(c)[0]: len(recommended_items_per_k[c].iloc[0]) for c in coverage_columns}

        hits_ratios = hits_ratios.merge(items_per_user, left_index=True, right_index=True)
        results = []
        for recall_column in recall_columns:
            for beta_column in beta_columns:
                k, beta = regex.findall(recall_column)
                if beta_column in recall_column and beta in regex.findall(beta_column):
                    results.append({
                        "k": k,
                        "beta": beta,
                        "recommended-items": recommended_items_per_k[k],
                        "recall": (hits_ratios[recall_column] * hits_ratios[beta_column]).sum()
                    })
        return pd.DataFrame.from_dict(results)

    @staticmethod
    def item_knn(interactions: list, similar_items: dict, k: int) -> list:
        """
        Item-kNN based on similarities in similar_items dictionary

        Parameters
        ----------
        interactions : list
            List of tuples (item_id, rating) to use for recommendation.
        similar_items : dict
            Dictionary where key is 'item_id' and value is list of nearest neighbors along with their similarity.
            Example: 'item_id_1' [(item_id_2, 0.78), (item_id_3, 0.77), ...].

        Returns
        -------
        list
            List of IDs of recommended items.
        """
        total = Counter()
        for interaction_value, item_id in interactions:
            total.update({i: r * interaction_value for i, r in similar_items.get(item_id, [])})
        return total.most_common(k)
